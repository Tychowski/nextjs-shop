import { useState } from "react";
import Router from "next/router";

import Layout from "../components/Layout";
import RowStyle from "../components/prebuilt/RowStyle";
import DonutShop from "../components/prebuilt/DonutShop";
import CheckoutForm from "../components/CheckoutForm";
import { 
  getDonutPrice,
  getDonutPrice2,
  getDonutPrice3,
  getDonutPrice4,
  getDonutPrice5,
  getDonutPrice6
 } from "../utils/setPrice";
import TreeRowStyle from "../components/prebuilt/TreeRowStyle";
import styled from "@emotion/styled";
import 'bootstrap/dist/css/bootstrap.css';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';



const MainPage = props => {
  const [numDonuts, setNumDonuts] = useState(1);
  const [numDonuts2, setNumDonuts2] = useState(1);
  const [numDonuts3, setNumDonuts3] = useState(1);
  const [numDonuts4, setNumDonuts4] = useState(1);
  const [numDonuts5, setNumDonuts5] = useState(1);
  const [numDonuts6, setNumDonuts6] = useState(1);

  const [sizeS, setSizeS] = useState(false);




  const addDonut = () => setNumDonuts(num => Math.min(12, num + 1));
  const remDonut = () => setNumDonuts(num => Math.max(1, num - 1));

  const addDonut2 = () => setNumDonuts2(num => Math.min(12, num + 1));
  const remDonut2 = () => setNumDonuts2(num => Math.max(1, num - 1));

  const addDonut3 = () => setNumDonuts3(num => Math.min(12, num + 1));
  const remDonut3 = () => setNumDonuts3(num => Math.max(1, num - 1));

  const addDonut4 = () => setNumDonuts4(num => Math.min(12, num + 1));
  const remDonut4 = () => setNumDonuts4(num => Math.max(1, num - 1));

  const addDonut5 = () => setNumDonuts5(num => Math.min(12, num + 1));
  const remDonut5 = () => setNumDonuts5(num => Math.max(1, num - 1));

  const addDonut6 = () => setNumDonuts6(num => Math.min(12, num + 1));
  const remDonut6 = () => setNumDonuts6(num => Math.max(1, num - 1));

  const changeSize = async () => {
    setSizeS(!sizeS);
  }


  const Divs = styled.div`
    margin: 5px;
  `;

  return (
    <>
    <br />
    <br />
    <br />
    <Container>
      <Row>
        <TreeRowStyle>
          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki'}
                    priceProduct={'2899 zł'}
                    pathImage={"./norek-1.png"}
                    onAddDonut={addDonut}
                    onRemoveDonut={remDonut}
                    numDonuts={numDonuts}
                    sizeS={() => onClick={changeSize}}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice(numDonuts)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 2'}
                    priceProduct={'3099 zł'}
                    pathImage={"./norek-2.png"}
                    onAddDonut={addDonut2}
                    onRemoveDonut={remDonut2}
                    numDonuts={numDonuts2}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice2(numDonuts2)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 3'}
                    priceProduct={'3299 zł'}
                    pathImage={"./norek-3.png"}
                    onAddDonut={addDonut3}
                    onRemoveDonut={remDonut3}
                    numDonuts={numDonuts3}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice3(numDonuts3)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>
        </TreeRowStyle>
      </Row>

      <br />
      <br /> 
      <br /> 

      <Row>
        <TreeRowStyle>
          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 4'}
                    priceProduct={'3399 zł'}
                    pathImage={"./norek-4.png"}
                    onAddDonut={addDonut4}
                    onRemoveDonut={remDonut4}
                    numDonuts={numDonuts4}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice4(numDonuts4)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 5'}
                    priceProduct={'3499 zł'}
                    pathImage={"./norek-5.png"}
                    onAddDonut={addDonut5}
                    onRemoveDonut={remDonut5}
                    numDonuts={numDonuts5}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice5(numDonuts5)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 6'}
                    priceProduct={'5000 zł'}
                    pathImage={"./norek-6.png"}
                    onAddDonut={addDonut6}
                    onRemoveDonut={remDonut6}
                    numDonuts={numDonuts6}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice6(numDonuts6)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>
        </TreeRowStyle>
      </Row>

      <br />
      <br />
      <br />

      <Row>
        <TreeRowStyle>
          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 7'}
                    priceProduct={'3399 zł'}
                    pathImage={"./norek-7.png"}
                    onAddDonut={addDonut4}
                    onRemoveDonut={remDonut4}
                    numDonuts={numDonuts4}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice4(numDonuts4)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 8'}
                    priceProduct={'3499 zł'}
                    pathImage={"./norek-8.png"}
                    onAddDonut={addDonut5}
                    onRemoveDonut={remDonut5}
                    numDonuts={numDonuts5}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice5(numDonuts5)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 9'}
                    priceProduct={'5000 zł'}
                    pathImage={"./norek-9.png"}
                    onAddDonut={addDonut6}
                    onRemoveDonut={remDonut6}
                    numDonuts={numDonuts6}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice6(numDonuts6)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>
        </TreeRowStyle>
      </Row>

      <br />
      <br />
      <br />

      <Row>
        <TreeRowStyle>
          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 10'}
                    priceProduct={'3399 zł'}
                    pathImage={"./norek-10.png"}
                    onAddDonut={addDonut4}
                    onRemoveDonut={remDonut4}
                    numDonuts={numDonuts4}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice4(numDonuts4)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 11'}
                    priceProduct={'3499 zł'}
                    pathImage={"./norek-11.png"}
                    onAddDonut={addDonut5}
                    onRemoveDonut={remDonut5}
                    numDonuts={numDonuts5}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice5(numDonuts5)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 12'}
                    priceProduct={'5000 zł'}
                    pathImage={"./norek-12.png"}
                    onAddDonut={addDonut6}
                    onRemoveDonut={remDonut6}
                    numDonuts={numDonuts6}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice6(numDonuts6)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>
        </TreeRowStyle>
      </Row>

      <br />
      <br />
      <br />

      <Row>
        <TreeRowStyle>
          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 13'}
                    priceProduct={'3399 zł'}
                    pathImage={"./norek-13.png"}
                    onAddDonut={addDonut4}
                    onRemoveDonut={remDonut4}
                    numDonuts={numDonuts4}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice4(numDonuts4)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 14'}
                    priceProduct={'3499 zł'}
                    pathImage={"./norek-14.png"}
                    onAddDonut={addDonut5}
                    onRemoveDonut={remDonut5}
                    numDonuts={numDonuts5}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice5(numDonuts5)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 15'}
                    priceProduct={'5000 zł'}
                    pathImage={"./norek-15.png"}
                    onAddDonut={addDonut6}
                    onRemoveDonut={remDonut6}
                    numDonuts={numDonuts6}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice6(numDonuts6)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>
        </TreeRowStyle>
      </Row>

      <br />
      <br />
      <br />

      <Row>
        <TreeRowStyle>
          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 16'}
                    priceProduct={'3399 zł'}
                    pathImage={"./norek-16.png"}
                    onAddDonut={addDonut4}
                    onRemoveDonut={remDonut4}
                    numDonuts={numDonuts4}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice4(numDonuts4)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 17'}
                    priceProduct={'3499 zł'}
                    pathImage={"./norek-17.png"}
                    onAddDonut={addDonut5}
                    onRemoveDonut={remDonut5}
                    numDonuts={numDonuts5}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice5(numDonuts5)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 18'}
                    priceProduct={'5000 zł'}
                    pathImage={"./norek-18.png"}
                    onAddDonut={addDonut6}
                    onRemoveDonut={remDonut6}
                    numDonuts={numDonuts6}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice6(numDonuts6)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>
        </TreeRowStyle>
      </Row>

      <br />
      <br />
      <br />

      <Row>
        <TreeRowStyle>
          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 19'}
                    priceProduct={'3399 zł'}
                    pathImage={"./norek-19.png"}
                    onAddDonut={addDonut4}
                    onRemoveDonut={remDonut4}
                    numDonuts={numDonuts4}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice4(numDonuts4)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 20'}
                    priceProduct={'3499 zł'}
                    pathImage={"./norek-20.png"}
                    onAddDonut={addDonut5}
                    onRemoveDonut={remDonut5}
                    numDonuts={numDonuts5}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice5(numDonuts5)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>

          <Col xs="12" sm="12" md="12" lg="4" xl="4">
            <Divs>
              <Layout title="Donut Shop 777">
                <RowStyle>
                  <DonutShop
                    titleProduct={'Futro z norki 21'}
                    priceProduct={'5000 zł'}
                    pathImage={"./norek-21.png"}
                    onAddDonut={addDonut6}
                    onRemoveDonut={remDonut6}
                    numDonuts={numDonuts6}
                  />
                </RowStyle>
                <CheckoutForm
                  price={getDonutPrice6(numDonuts6)}
                  onSuccessfulCheckout={() => Router.push("/success")}
                />
              </Layout>
            </Divs>
          </Col>
        </TreeRowStyle>
      </Row>

      <br />
      <br />
      <br />

    </Container>
    </>
  );
};

export default MainPage;

