export const getDonutPrice = numDonuts =>
  (Math.round(numDonuts * 2899 * 100) / 100).toFixed(2);

export const getDonutPrice2 = numDonuts2 =>
  (Math.round(numDonuts2 * 3099 * 100) / 100).toFixed(2);

export const getDonutPrice3 = numDonuts3 =>
  (Math.round(numDonuts3 * 3299 * 100) / 100).toFixed(2);

export const getDonutPrice4 = numDonuts4 =>
  (Math.round(numDonuts4 * 3399 * 100) / 100).toFixed(2);

export const getDonutPrice5 = numDonuts5 =>
  (Math.round(numDonuts5 * 3499 * 100) / 100).toFixed(2);

export const getDonutPrice6 = numDonuts6 =>
  (Math.round(numDonuts6 * 5000 * 100) / 100).toFixed(2);


// get-donut-price.js