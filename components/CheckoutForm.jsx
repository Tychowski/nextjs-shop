import { useState } from "react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import styled from "@emotion/styled";
import axios from "axios";

import RowStyle from "./prebuilt/RowStyle";
import RowStyleWithoutBg from "./prebuilt/RowStyleWithoutBg";
import BuyClose from "./prebuilt/BuyClose";


import BillingDetailsFields from "./prebuilt/BillingDetailsFields";
import SubmitButton from "./prebuilt/SubmitButton";
import CheckoutError from "./prebuilt/CheckoutError";
import TreeRowStyle from "./prebuilt/TreeRowStyle";

const CardElementContainer = styled.div`
  height: 40px;
  display: flex;
  align-items: center;
  font-size: 12px;


  & .StripeElement {
    width: 100%;
    padding: 2px 5px;
  }

  &::placeholder {
    color: rgb(235, 235, 235);
  }
`;

const CheckoutForm = ({ price, onSuccessfulCheckout, sizeS }) => {
  const [isProcessing, setProcessingTo] = useState(false);
  const [checkoutError, setCheckoutError] = useState();
  const [showMe, setShowMe] = useState(false);


  const stripe = useStripe();
  const elements = useElements();

  // TIP
  // use the cardElements onChange prop to add a handler
  // for setting any errors:

  const handleCardDetailsChange = ev => {
    ev.error ? setCheckoutError(ev.error.message) : setCheckoutError();
  };

  const handleFormSubmit = async ev => {
    ev.preventDefault();

    const billingDetails = {
      name: ev.target.name.value,
      email: ev.target.email.value,
      address: {
        city: ev.target.city.value,
        line1: ev.target.address.value,
        state: ev.target.state.value,
        postal_code: ev.target.zip.value
      }
    };

    setProcessingTo(true);

    const cardElement = elements.getElement("card");

    try {
      const { data: clientSecret } = await axios.post("/api/payment_intents", {
        amount: price * 100, 
        size: sizeS
      });

      const paymentMethodReq = await stripe.createPaymentMethod({
        type: "card",
        card: cardElement,
        billing_details: billingDetails
      });

      if (paymentMethodReq.error) {
        setCheckoutError(paymentMethodReq.error.message);
        setProcessingTo(false);
        return;
      }

      const { error } = await stripe.confirmCardPayment(clientSecret, {
        payment_method: paymentMethodReq.paymentMethod.id
      });

      if (error) {
        setCheckoutError(error.message);
        setProcessingTo(false);
        return;
      }

      onSuccessfulCheckout();
    } catch (err) {
      setCheckoutError(err.message);
    }
  };

  const toggle = async () => {
    setShowMe(!showMe);
  }

  const iframeStyles = {
    base: {
      color: "#fff",
      fontSize: "15px",
      iconColor: "#fff",
      "::placeholder": {
        color: "rgb(235, 235, 235)"
      }
    },
    invalid: {
      iconColor: "rgb(253, 7, 81)",
      color: "rgb(253, 7, 81)"
    },
    complete: {
      iconColor: "rgb(97, 190, 74)"
    }
  };

  const cardElementOpts = {
    iconStyle: "solid",
    style: iframeStyles,
    hidePostalCode: true
  };

  return (
    <>
    <RowStyleWithoutBg>
        <BuyClose onClick={toggle}>
          Kup
        </BuyClose>
    </RowStyleWithoutBg>

    <div className="paymentForm" style={{ display: showMe?"block":"none"}}>
    <button className="btnClose" onClick={toggle}>x</button>

      <form onSubmit={handleFormSubmit}>
        <RowStyle>
          <BillingDetailsFields />
        </RowStyle>
        <RowStyle>
          <CardElementContainer>
            <CardElement
              options={cardElementOpts}
              onChange={handleCardDetailsChange}
            />
          </CardElementContainer>
        </RowStyle>
        {checkoutError && <CheckoutError>{checkoutError}</CheckoutError>}
        <RowStyle>
          <SubmitButton disabled={isProcessing || !stripe}>
            {isProcessing ? "Processing..." : `Zapłać ${price}zł`}
          </SubmitButton>
        </RowStyle>
      </form>

      <style jsx>{`
        .paymentForm {
          position: fixed !important;
          top: 10%;
          left: 20%;
          padding: 30px 50px;
          width: 60%;
          z-index: 999 !important;
          background: #23b477e7;
        }

        .btnClose {
          top: 0;
          margin: 5px 5px 0px 0px;
          color: #fff;
          background: rgba(255, 0, 76, 0.89);
          position: absolute; 
          right: 0;
          padding: 5px 15px;
        }

        .changeSize {
          font-size: 16px;
        }

        @media only screen and (max-width: 992px) {
          .changeSize {
            font-size: 16px;
          }  
        }

        @media only screen and (max-width: 992px) {
          .paymentForm {
            left: 15%;
            width: 70%;
            padding: 10px 5px;
          }  
        }

        @media only screen and (max-width: 500px) {
          .paymentForm {
            padding-top: 40px;
          }  
        }
      `}</style>
    </div>
    </>
  );
};

export default CheckoutForm;
