import styled from "@emotion/styled";

const SizeM = styled.button`
    text-align: center;
    color: #333;
    font-weight: 400;
    font-size: 15px;
    margin: 2px 15px;
    padding: 0px 8px;

    &:focus {
        background: rgba(0, 140, 255, 0.267);
    }
`;

export default SizeM;

