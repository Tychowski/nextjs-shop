import styled from "@emotion/styled";

const ProductTitle = styled.span`
    text-align: center;
    height: 40px;
    color: #333;
    font-weight: 200;
    border: 1px solid #333;

    display: flex;
	flex-direction: RowStyle;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
	align-content: center;
`;

export default ProductTitle;

