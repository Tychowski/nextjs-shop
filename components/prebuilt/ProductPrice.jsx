import styled from "@emotion/styled";

const ProductPrice = styled.span`
    text-align: center;
    height: 20px;
    color: #fff;
    font-weight: 400;
    margin-top: 10px;
    // border: 1px solid #333;

    display: flex;
	flex-direction: RowStyle;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
	align-content: center;
`;

export default ProductPrice;

