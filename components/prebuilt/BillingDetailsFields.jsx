import FormField from "./FormField";

const BillingDetailsFields = () => {
  return (
    <>
      <FormField
        name="name"
        label="Imie"
        type="text"
        placeholder="Jan Kowalski"
        required
      />
      <FormField
        name="email"
        label="Email"
        type="email"
        placeholder="kowalski@example.com"
        required
      />
      <FormField
        name="address"
        label="Adres"
        type="text"
        placeholder="ul. Studencka 17"
        required
      />
      <FormField
        name="city"
        label="Miasto"
        type="text"
        placeholder="Warszawa"
        required
      />
      <FormField
        name="state"
        label="Kraj"
        type="text"
        placeholder="Polska"
        required
      />
      <FormField
        name="zip"
        label="ZIP"
        type="text"
        placeholder="31-001"
        required
      />
      <FormField
        name="dostawa"
        label="Dostawa"
        type="disabled"
        placeholder="Kurier InPost"
        disabled={true} 
      />
    </>
  );
};

export default BillingDetailsFields;
