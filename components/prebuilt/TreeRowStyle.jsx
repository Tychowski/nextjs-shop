import styled from "@emotion/styled";

const TreeRowStyle = styled.div`
    display: flex;
    flex-direction: RowStyle;
    flex-wrap: wrap;
    justify-content: space-around;
    background-color: #fff;
    width: 100%;
`;

export default TreeRowStyle;

