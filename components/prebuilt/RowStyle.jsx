import styled from "@emotion/styled";

const RowStyle = styled.div`
  margin: 10px auto;
  box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 #d1d1d1;
  border-radius: 4px;
  background-color: rgb(189, 189, 189);
  position: relative;

  @media screen and (min-width: 350px) and (max-width: 500px) {
    width: 95%;
  }

  @media screen and (min-width: 500px) and (max-width: 770px) {
    width: 70%;
  }

  @media screen and (min-width: 770px) and (max-width: 991px) {
    width: 50%;
  }
  
`;

export default RowStyle;
