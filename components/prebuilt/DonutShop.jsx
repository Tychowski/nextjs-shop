import styled from "@emotion/styled";

import { useState } from "react";
import Image from "./Image";
import DonutQuantity from "./DonutQuantity";
import RowStyleWithoutBg from "./RowStyleWithoutBg";
import BuyClose from "./BuyClose";
import ProductTitle from "./ProductTitle";
import ProductPrice from "./ProductPrice";
import ProductSize from "./ProductSize";
import SizeS from "./Size/SizeS";
import SizeM from "./Size/SizeM";
import SizeL from "./Size/SizeL";

const Shop = styled.div`
  padding: 10px 20px 40px 20px;
  border-top: none !important;
  background: rgb(212, 212, 212);  
`;

const ShopName = styled.h1`
  font-size: 18px;
  color: #fff;
  font-style: normal;
  font-variant: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 26.4px;
  margin-bottom: 20px;
`;

const Controls = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 40px;
`;

const DonutShop = ({ onAddDonut, onRemoveDonut, numDonuts, titleProduct, priceProduct, pathImage }) => {

  // const [sizeS, setSizeS] = useState(true);
  // const [sizeM, setSizeM] = useState(true);
  // const [status, setStatus] = useState(false);
  // const [status2, setStatus2] = useState(false);



  // const changeSizeS = async () => {
  //   setSizeS(sizeS);

  //   console.log("size S --> ", sizeS)
  // }

  // const changeSizeM = async () => {
  //   setSizeM(sizeM);

  //   console.log("size M --> ", sizeM)
  // }

  // const chan = async () => {
  //   setStatus(!status)
  //   setStatus2(status2)

  //   console.log("size S --> ", status)
  //   console.log("size M --> ", status2)

  // }


  return (
    <>
    <Shop>
      {/* <button onClick={chan}>
        {`Current status: ${status ? 'on' : 'off'}`}
      </button>
      <br />
      <br />
      <button onClick={() => setStatus2(!status2)}>
        {`Current status2: ${status2 ? 'on2' : 'off2'}`}
      </button> */}
      <ShopName>Rimma donna</ShopName>
      <Image src={pathImage}></Image>
      <RowStyleWithoutBg>
        <ProductTitle>
          {titleProduct}
        </ProductTitle>
        <ProductPrice>
          {priceProduct}
        </ProductPrice>
        <ProductSize>
          <SizeS>S</SizeS>
          <SizeM>M</SizeM>
          <SizeL>L</SizeL>
        </ProductSize>
      </RowStyleWithoutBg>
      <Controls>
        <DonutQuantity
          onAdd={onAddDonut}
          onRemove={onRemoveDonut}
          quantity={numDonuts}
        />
      </Controls>
    </Shop>
    </>
  );
};

export default DonutShop;
