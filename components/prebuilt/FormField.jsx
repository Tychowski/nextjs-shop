import styled from "@emotion/styled";

const FormFieldContainer = styled.div`
  display: -ms-flexbox;
  display: flex;
  -ms-flex-align: center;
  align-items: center;
  margin-left: 15px;
  border-top: 1px solid rgba(51, 51, 51, 0.39);

  &:first-of-type {
    border-top: none;
  }
`;

const Label = styled.label`
  width: 20%;
  min-width: 70px;
  padding: 11px 0;
  color: rgb(235, 235, 235);
  overflow: hidden;
  font-size: 16px;
  text-overflow: ellipsis;
  white-space: nowrap;
  border-right: 1px solid rgba(51, 51, 51, 0.39);

  @media only screen and (max-width: 500px) {
    font-size: 14px;
  }
`;

const Input = styled.input`
  font-size: 16px;
  width: 100%;
  padding: 11px 15px 11px 8px;
  color: #fff;
  background-color: transparent;
  animation: 1ms void-animation-out;

  &::placeholder {
    color: rgba(224, 224, 224, 0.705);
  }

  @media only screen and (max-width: 500px) {
    font-size: 14px;
  }
`;

const FormField = ({ label, type, name, placeholder, disabled, required }) => {
  return (
    <FormFieldContainer>
      <Label htmlFor={name}>{label}</Label>
      <Input name={name} type={type} placeholder={placeholder} disabled={disabled} required />
    </FormFieldContainer>
  );
};

export default FormField;
