require("dotenv").config();

module.exports = {
  env: {
    PUBLISHABLE_KEY: process.env.PUBLISHABLE_KEY,
    TEST_LOL: process.env.TEST_LOL
  }
};
